#include <iostream>
#include <list>
#include <cstring>

using namespace std;

class Track{
private:
	std::string _name = "-- Untitled --";
	unsigned int _duration = 0u;
	std::string _master = "";

public:
	Track(){}
	std::string title() {
		return _name;
	}
	void title(std::string nameLocal) {
		_name = nameLocal;
	}
	unsigned int duration() {
		return _duration;
	}
	void duration(unsigned int duration) {
		_duration = duration;
	}
	std::string master() {
		return _master;
	}
	void master(std::string master) {
		_master = master;
	}
};

typedef list<Track*> Tracks;

int main(void){
	Tracks _cancion;
	int _num_canciones = 0;
	string _name_cancion;
	int _stat = 0;
	cin >> _num_canciones;

	for (int i = 0; i < _num_canciones; i++) {
		getline(cin, _name_cancion, '-');
		_name_cancion.erase(_name_cancion.begin(),_name_cancion.begin()+1);
		cin >> _stat;

		Track* _cancion_obj = new Track();
		_cancion_obj->title(_name_cancion);
		_cancion_obj->duration(_stat);

		_cancion.push_back(_cancion_obj);
	}

	for (Tracks::iterator it = _cancion.begin(); it != _cancion.end(); it++) {
		cout << "Track: " << (*it)->title() << " [" << (*it)->duration() << "s]\n";
	}
}
