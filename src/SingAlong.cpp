#include <list>
#include <iostream>
#include <cstring>
#include <fstream>
#include "Exceptions.h"
#include "Artist.h"
#include "ConverterGroup.hxx"
#include "User.h"



using namespace std;
typedef list<Artist*> Artists;
typedef list<User*> Users;


class SingAlong{

private:
	Artists _artistas;
	Tracks _tracks;
	Users _users;
	ConverterGroup Cgroup;



public:
	SingAlong(){
		Cgroup.addConvertGroup("mp3" , 128 );
		Cgroup.addConvertGroup("mp3" , 192 );
		Cgroup.addConvertGroup("ogg" , 128 );
		Cgroup.addConvertGroup("ogg" , 192 );
		Cgroup.addConvertGroup("ogg" , 96 );
		Cgroup.addConvertGroup("mp3" , 96 );
	
	}
	~SingAlong(){
		for (Artists::iterator it = _artistas.begin(); it != _artistas.end(); ++it) {
			delete(*it);
		}
		_artistas.clear();
		for (Users::iterator it = _users.begin(); it != _users.end(); ++it) {
			delete(*it);
		}
		_users.clear();
	}

	string catalog() {
		string answer = "";
		for (Artists::iterator it = _artistas.begin(); it != _artistas.end(); ++it) {
			answer.append((*it)->description());
			answer.append((*it)->tracks());
			answer.append((*it)->albums());
		}
		return answer;
	}

	void createArtist(string nameLocal, bool singleLocal){
		Artist* _artist_obj = new Artist();
		_artist_obj->name(nameLocal);
		if(singleLocal){
			_artist_obj->beGroup();
		}else{
			_artist_obj->beSolo();
		}
		_artistas.push_back(_artist_obj);
	}

	Artist& findArtist(string nameLocal){
		for (Artists::iterator it = _artistas.begin(); it != _artistas.end(); ++it) {
			if(nameLocal.compare((*it)->name()) == 0){
				return **it;
			}
		}
		throw ArtistNotFoundExcepion();
	}

	void createNewTrack(string nameLocal, string songLocal, string masterLocal){
		Artist & artist = findArtist(nameLocal);
		char data[100];
		fstream  afile;
		afile.open("masters/"+masterLocal, ios::in );
		if(afile) {
			afile >> data;
			afile.close();
			unsigned int val = stoul (data,nullptr,0);
			artist.addNewTrack(songLocal, masterLocal, val);
			//mailstub.sendMail(const std::string& to, const std::string& subject );
			artist.sendMail(songLocal);
		}
		else {
		   throw MasterNotFoundExcepion();
		}
		
		Cgroup.convert(("masters/"+masterLocal), ("compressed/"+nameLocal + " - "+ songLocal));
	}
	void createNewTrack(string nameLocal, string songLocal, string masterLocal, unsigned int duration){
		Artist & artist = findArtist(nameLocal);
		artist.addNewTrack(songLocal, masterLocal, duration);
	}
	void createNewAlbum(string nameLocal, string albumLocal){
		Artist & artist = findArtist(nameLocal);
		artist.addNewAlbum(albumLocal);
	}
	void listAlbum(string nameLocal, string albumLocal){
		Artist & artist = findArtist(nameLocal);
		Album & album = artist.findAlbum(albumLocal);
		album.list();
	}
	void unlistAlbum(string nameLocal, string albumLocal){
		Artist & artist = findArtist(nameLocal);
		Album & album = artist.findAlbum(albumLocal);
		album.unlist();
	}

	void includeTrackOnAlbum(string nameLocal, string trackLocal, string albumLocal){
		Artist & artist = findArtist(nameLocal);
		Album & album = artist.findAlbum(albumLocal);
		Track & track = artist.findTrack(trackLocal);
		album.addTrack(&track);
	} 
	
	void createNewUser(string username, string mail){
		User* _user_obj = new User();
		_user_obj->name(username);
		_user_obj->mail(mail);
		_users.push_back(_user_obj);
	}
	string userList(){
		string answer = "";
		for (Users::iterator it = _users.begin(); it != _users.end(); ++it) {
			answer.append( (*it)->data());
		}
		return answer;
	}
	User& findUser(string nameLocal){
		for (Users::iterator it = _users.begin(); it != _users.end(); ++it) {
			if(nameLocal.compare((*it)->name()) == 0){
				return **it;
			}
		}
		throw UserNotFoundException();
	}
	void subscribeUserToArtist(string username, string artistname){
		Artist & artist = findArtist(artistname);
		User & user = findUser(username);
		artist.subscribeUser(&user);
	}
	void subscribeUserToStyle(string username, string artistname){
		User & user = findUser(username);
		Artist & artist = findArtist(artistname);
		artist.subscribeUser(&user);
	}
	
	string listSubscribedToArtist(string artistname){
		Artist & artist = findArtist(artistname);
		return artist.subscribers();
	}
};
