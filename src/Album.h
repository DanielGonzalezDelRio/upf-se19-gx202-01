#ifndef Album_h
#define Album_h
#include <list>
#include <iostream>
#include <cstring>
#include "Track.h"
using namespace std;
typedef list<Track*> Tracks;
class Album{
private:
	string _name = "-- Untitled --";
	bool _listed = false;
	Tracks _tracks;
public:
	string title();
	void title(string nameLocal);
	bool isListed();
	void list();
	void unlist();
	void addTrack( Track * track);
	string tracks();
	~Album();
};
#endif