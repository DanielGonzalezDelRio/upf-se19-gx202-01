#ifndef OggConverter_h
#define OggConverter_h
#include <list>
#include <iostream>
#include <string>
#include <cstring>
#include <fstream>
#include "Converter.hxx"
#include "Exceptions.h"
#include "LibFileSystem.hxx"
#include "OggVorbisEncoder.hxx"
using namespace std;

class OggConverter : public Converter {
private:
  int _Rate = 128;
public /*virtual Converter{}*/:
  void setUp();
  void tearDown();
  void createMasterFile( const std::string & name, int duration );
  void convert(string name, string name2);
  void bitRate(int rate);
};
#endif
