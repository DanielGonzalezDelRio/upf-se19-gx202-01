#include "ConverterGroup.hxx"
void ConverterGroup::addConvertGroup ( string _name, int btr){
	if(_name.compare("mp3") == 0){
		Mp3Converter * _converter_obj = new Mp3Converter();
		_converter_obj->bitRate(btr);
		_converters.push_back(_converter_obj);
	}else if(_name.compare("ogg") == 0){
		OggConverter * _converter_obj = new OggConverter();
		_converter_obj->bitRate(btr);
		_converters.push_back(_converter_obj);
	}else{
		throw UnsupportedFormatException();
	}
}
void ConverterGroup::convert(string name, string name2){

	for (Converters::iterator it = _converters.begin(); it != _converters.end(); it++) {
		(*it)->convert( name, name2 ); //
	}
}
ConverterGroup::~ConverterGroup(){
	for (Converters::iterator it = _converters.begin(); it != _converters.end(); it++) {
		delete(*it);		
	}
	_converters.clear();
}