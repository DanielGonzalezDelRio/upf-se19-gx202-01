#ifndef Exception_h
#define Exception_h

class ArtistNotFoundExcepion : public std::exception {
public:
	const char * what() const throw () {
		return "The artist does not exist";
	}
};

class MasterNotFoundExcepion : public std::exception {
public:
	const char * what() const throw () {
		return "The master file does not exist";
	}
};

class FakeMasterNotFoundExcepion : public std::exception {
public:
	const char * what() const throw () {
		return "The master file does not exist";
	}
};

class AlbumNotFoundExcepion : public std::exception {
public:
	const char * what() const throw () {
		return "The album does not exist";
	}
};

class TrackNotFoundExcepion : public std::exception {
public:
	const char * what() const throw () {
		return "The album does not exist";
	}
};

class UnsupportedFormatException : public std::exception {
public:
	const char * what() const throw () {
		return "Unsupported format";
	}
};

class UserNotFoundException : public std::exception {
public:
	const char * what() const throw () {
		return "The user does not exist";
	}
};



#endif
