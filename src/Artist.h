#ifndef Artist_h
#define Artist_h
#include <list>
#include <iostream>
#include <cstring>
#include "Track.h"
#include "Album.h"
#include "User.h"
#include "MailStub.hxx"
using namespace std;
typedef list<Track*> Tracks;
typedef list<Album*> Albums;
typedef list<User*> Users;

class Artist{
private:
	string _name = "-- Untitled --";
	bool _single = true;
	Tracks _tracks;
	Albums _albums;
	Users _users;
	MailStub & mailstub = MailStub::theInstance();

public:
	string name();
	void name(string nameLocal);
	bool isSingle();
	void beSolo();
	void beGroup();
	string description();
	void addNewTrack(string songLocal, string masterLocal, unsigned int duration);
	string tracks();
	void addNewAlbum(string albumLocal);
	string albums();
	Album& findAlbum(string nameLocal);
	Track& findTrack(string trackLocal);
	void subscribeUser(User * user);
	string subscribers();
	void sendMail(string name);
	~Artist();
};

#endif
