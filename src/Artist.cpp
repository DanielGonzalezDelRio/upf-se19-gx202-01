#include "Artist.h"
#include "Exceptions.h"


	string Artist::name() {
		return _name;
	}
	void Artist::name(string nameLocal) {
		_name = nameLocal;
	}
	bool Artist::isSingle() {
		return _single;
	}
	void Artist::beSolo() {
		_single = true;
	}
	void Artist::beGroup() {
		_single = false;
	}
	string Artist::description() {
		if(_single){
			return _name+" [solo]\n";
		}else{
			return _name+" [group]\n";
		}
	}
	Artist::~Artist(){
		for (Tracks::iterator it = _tracks.begin(); it != _tracks.end(); ++it) {
			delete(*it);
		}
		_tracks.clear();
		for (Albums::iterator it = _albums.begin(); it != _albums.end(); ++it) {
			delete(*it);
		}
		_albums.clear();
	}
	
	void Artist::addNewTrack( string songLocal, string masterLocal, unsigned int duration){
		Track* _track_obj = new Track();
		_track_obj->title(songLocal);
		_track_obj->master(masterLocal);
		_track_obj->duration(duration);
		_tracks.push_back(_track_obj);
	}
	
	string Artist::tracks() {
		string answer = "";
		for (Tracks::iterator it = _tracks.begin(); it != _tracks.end(); ++it) {
			answer.append("\t"+(*it)->title()+ " ["+to_string((*it)->duration())+"s]"+"\n"+"\t\tmasters/"+(*it)->master()+"\n");
		}
		return answer;
	}
	
	void Artist::addNewAlbum(string albumLocal){
		Album* _album_obj = new Album();
		_album_obj->title(albumLocal);
		_albums.push_back(_album_obj);
	}
	
	string Artist::albums() {
		string answer = "";
		for (Albums::iterator it = _albums.begin(); it != _albums.end(); ++it) {
			if((*it)->isListed()==true){
				answer.append("Album: "+(*it)->title()+"\n");
			}else{
				answer.append("Album: "+(*it)->title()+" [unlisted]\n");
			}
			answer.append((*it)->tracks());
		}
		return answer;
	}
	Album& Artist::findAlbum(string nameLocal){
		for (Albums::iterator it = _albums.begin(); it != _albums.end(); ++it) {
			if(nameLocal.compare((*it)->title()) == 0){
				return **it;
			}
		}
		throw AlbumNotFoundExcepion();
	}
	
	////////////hacer test para esta clase
	Track& Artist::findTrack(string trackLocal){
		for (Tracks::iterator it = _tracks.begin(); it != _tracks.end(); ++it) {
			if(trackLocal.compare((*it)->title()) == 0){
				return **it;
			}
		}
		throw TrackNotFoundExcepion();
	}
	
	void Artist::subscribeUser( User * user){
		_users.push_back(user);
	}
	
	string Artist::subscribers(){
		string answer = "";
		for (Users::iterator it = _users.begin(); it != _users.end(); ++it) {
			answer.append((*it)->name()+"\n");
		}
		return answer;
	}
	
	void Artist::sendMail(string name){
		for (Users::iterator it = _users.begin(); it != _users.end(); ++it) {		
			mailstub.sendMail((*it)->name()+" <"+(*it)->mail()+">", "new track "+name+" by "+_name+"" );
		}			
	}
	