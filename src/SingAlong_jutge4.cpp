#ifndef Track_h
#define Track_h
#include <iostream>
using namespace std;

class ArtistNotFoundExcepion : public std::exception {
public:
	const char * what() const throw () {
		return "The artist does not exist";
	}
};

class MasterNotFoundExcepion : public std::exception {
public:
	const char * what() const throw () {
		return "The master file does not exist";
	}
};

class AlbumNotFoundExcepion : public std::exception {
public:
	const char * what() const throw () {
		return "The album does not exist";
	}
};

class TrackNotFoundExcepion : public std::exception {
public:
	const char * what() const throw () {
		return "The album does not exist";
	}
};


class Track{
private:
	std::string _name = "-- Untitled --";
	unsigned int _duration = 0u;
	std::string _master = "";

public:
	std::string title();
	void title(std::string nameLocal);
	unsigned int duration();
	void duration(unsigned int duration);
	std::string master();
	void master(std::string master);
};
#endif
	std::string Track::title() {
		return _name;
	}
	void Track::title(std::string nameLocal) {
		_name = nameLocal;
	}
	unsigned int Track::duration() {
		return _duration;
	}
	void Track::duration(unsigned int duration) {
		_duration = duration;
	}
	std::string Track::master() {
		return _master;
	}
	void Track::master(std::string master) {
		_master = master;
	}
#ifndef Album_h
#define Album_h
#include <list>
#include <iostream>
#include <cstring>
using namespace std;
typedef list<Track*> Tracks;


class Album{
private:
	string _name = "-- Untitled --";
	bool _listed = false;
public:
    Tracks _tracks;

    string title();
	void title(string nameLocal);
	bool isListed();
	void list();
	void unlist();
	void addTrack( Track * track);
	string tracks();
};
#endif


	string Album::title() {
		return _name;
	}
	void Album::title(string nameLocal) {
		_name = nameLocal;
	}
	bool Album::isListed() {
		return _listed;
	}
	void Album::list() {
		_listed = true;
	}
	void Album::unlist() {
		_listed = false;
	}


	///crear test para estas clases
	void Album::addTrack( Track * track){
		_tracks.push_back(track);
	}
	string Album::tracks() {
		string answer = "";
		int count = 1;
		for (Tracks::iterator it = _tracks.begin(); it != _tracks.end(); ++it) {
			answer.append(to_string(count)+" - "+(*it)->title()+ " ["+to_string((*it)->duration())+"s]\n");
			count++;
		}
		return answer;
	}

#ifndef Artist_h
#define Artist_h
#include <list>
#include <iostream>
#include <cstring>
#include <vector>


using namespace std;
typedef list<Track*> Tracks;
typedef list<Album*> Albums;

class Artist{
private:
	string _name = "-- Untitled --";
	bool _single = true;
public:

	vector<string> _users;
    Albums _albums;
    Tracks _tracks;

	string name();
	void name(string nameLocal);
	bool isSingle();
	void beSolo();
	void beGroup();
	string description();
	void addNewTrack(string songLocal, string masterLocal, unsigned int duration);
	string tracks();
	void addNewAlbum(string albumLocal);
	void addNewUser(string userLocal);
	string albums();
	string users();
	Album& findAlbum(string nameLocal);
	Track& findTrack(string trackLocal);
};

#endif

	string Artist::name() {
		return _name;
	}
	void Artist::name(string nameLocal) {
		_name = nameLocal;
	}
	bool Artist::isSingle() {
		return _single;
	}
	void Artist::beSolo() {
		_single = true;
	}
	void Artist::beGroup() {
		_single = false;
	}
	string Artist::description() {
		_name.erase(_name.begin(),_name.begin()+1);
		if(_single){
			return _name+" [solo]\n";
		}else{
			return _name+" [group]\n";
		}
	}

	void Artist::addNewTrack( string songLocal, string masterLocal, unsigned int duration){
		Track* _track_obj = new Track();
		_track_obj->title(songLocal);
		_track_obj->master(masterLocal);
		_track_obj->duration(duration);
		_tracks.push_back(_track_obj);
	}

	string Artist::tracks() {
		string answer = "";
		for (Tracks::iterator it = _tracks.begin(); it != _tracks.end(); ++it) {
			answer.append("\t"+(*it)->title()+ " ["+to_string((*it)->duration())+"s]"+"\n"+"\t\t"+(*it)->master()+"\n");
		}
		return answer;
	}
	void Artist::addNewUser(string userLocal){
		_users.push_back(userLocal);
		
	}
	string Artist::users() {
		string answer = "";
		for (vector<string>::iterator it = _users.begin(); it != _users.end(); ++it) {
			answer.append((*it)+"\n");
			
		}
		return answer;
	}
	void Artist::addNewAlbum(string albumLocal){
		Album* _album_obj = new Album();
		_album_obj->title(albumLocal);
		_albums.push_back(_album_obj);
	}

	string Artist::albums() {
		string answer = "";
		for (Albums::iterator it = _albums.begin(); it != _albums.end(); ++it) {
			if((*it)->isListed()==true){
				answer.append("Album: "+(*it)->title()+"\n");
			}else{
				answer.append("Album: "+(*it)->title()+" [unlisted]\n");
			}
			answer.append((*it)->tracks());
		}
		return answer;
	}
	Album& Artist::findAlbum(string nameLocal){
		for (Albums::iterator it = _albums.begin(); it != _albums.end(); ++it) {
			if(nameLocal.compare((*it)->title()) == 0){
				return **it;
			}
		}
		throw AlbumNotFoundExcepion();
	}

	////////////hacer test para esta clase
	Track& Artist::findTrack(string trackLocal){
		for (Tracks::iterator it = _tracks.begin(); it != _tracks.end(); ++it) {
			if(trackLocal.compare((*it)->title()) == 0){
				return **it;
			}
		}
		throw TrackNotFoundExcepion();
	}
#include <list>
#include <iostream>
#include <cstring>
using namespace std;
typedef list < Artist * > Artists;

class SingAlong {
private:
    Artists _artistas;
    Tracks _tracks;

public:
    string catalog()
    {
        string answer = "";
        for (Artists::iterator it = _artistas.begin(); it != _artistas.end(); it++) {
            answer.append((*it)->description());
            answer.append((*it)->tracks());
            answer.append((*it)->albums());
			answer.append((*it)->users());
            //delete(*it);
        }
        return answer;
    }

    void createArtist(string nameLocal, bool singleLocal)
    {
        Artist *_artist_obj = new Artist();
        _artist_obj->name(nameLocal);
        if (singleLocal) {
            _artist_obj->beGroup();
        } else {
            _artist_obj->beSolo();
        }
		//_artist_obj->addNewUser("user");
		//_artist_obj->addNewUser("patata");
        _artistas.push_back(_artist_obj);
    }

    Artist& findArtist(string nameLocal)
    {
        for (Artists::iterator it = _artistas.begin(); it != _artistas.end(); it++) {
            if (nameLocal.compare((*it)->name()) == 0) {
                return **it;
            }
        }
        throw ArtistNotFoundExcepion();
    }

		void createNewTrack(string nameLocal, string albumLocal, string artistaLocal, int durationLocal, string masterLocal)                // NEW
{
		Artist & artist = findArtist(artistaLocal);
		//Album & album = artist.findAlbum(albumLocal);
		artist.addNewTrack(nameLocal, masterLocal, durationLocal);
		//album.addTrack(trackLocal);
}

    void createNewAlbum(string nameLocal, string albumLocal)
    {
        Artist & artist = findArtist(nameLocal);
        artist.addNewAlbum(albumLocal);
    }
	void createNewUser(string nameLocal, string artistLocal)
    {
		string aux = "\n";
		aux.append(artistLocal);
        Artist & artist = findArtist(aux);
        artist.addNewUser(nameLocal);
    }

    void listAlbum(string nameLocal, string albumLocal)
    {
        Artist & artist = findArtist(nameLocal);
        Album & album = artist.findAlbum(albumLocal);
        album.list();
    }

    void unlistAlbum(string nameLocal, string albumLocal)
    {
        Artist & artist = findArtist(nameLocal);
        Album & album = artist.findAlbum(albumLocal);
        album.unlist();
    }

    void includeTrackOnAlbum(string nameLocal, string trackLocal, string albumLocal)
    {
        //antes de continuar modificar album con lista de tracks y hacer test nuevos, pasarlosy volver aqui. e.e
        Artist & artist = findArtist(nameLocal);
        Album & album = artist.findAlbum(albumLocal);
        Track & track = artist.findTrack(trackLocal);
        album.addTrack(&track);
    }
};

int main(void)
{

    SingAlong _singalone;
    Artists _artistas;
    int _num = 0;
    //int _stat = 0;
    int _duracio = 0;
    string _name_artista;
    string _name_album;
    string _name_track;
    string _name_file;
	string _name_user;
    char _stats = ' ';
    bool _bstat = false;
    cin >> _num;
    for (int i = 0; i < _num; i++) {
        getline(cin, _name_artista, '-');                                   // Grab text before -
        //_name_artista.erase(_name_artista.begin(), _name_artista.begin() + 1);      // Erase first caracther
        cin >> _stats;                                                       // Grab state
        if (_stats == 'Y') {
            _bstat = true;
        } else {
            _bstat = false;
        }
        _singalone.createArtist(_name_artista, _bstat);
    }
    cin >> _num;
    for (int i = 0; i < _num; i++) {
        getline(cin, _name_artista, '-');                                   // Grab text before -
        //_name_artista.erase(_name_artista.begin(),_name_artista.begin()+1); // Erase first caracther
        getline(cin, _name_album, '-');
        //_name_album.erase(_name_album.begin(), _name_album.begin() + 1);
        cin >> _stats;
        _singalone.createNewAlbum(_name_artista, _name_album);              // afegir is listed!!!!!!!!!!!!!!!!!!!!!!!!
				if (_stats == 'Y') {
            _singalone.listAlbum(_name_artista, _name_album);
        }
		}
    cin >> _num;
    for (int i = 0; i < _num; i++) {
        getline(cin, _name_artista, '-');   
		// Enters artist
        //_name_artista.erase(_name_artista.begin(),_name_artista.begin()+1); // Erase first caracther
        getline(cin, _name_album, '-');                                     // Enters album
        //_name_album.erase(_name_album.begin(), _name_album.begin() + 1);       // Erase first caracther
        getline(cin, _name_track, '-');                                     // Enters name song
        //_name_track.erase(_name_track.begin(), _name_track.begin() + 1);       // Erase fisrt caracther
        getline(cin, _name_file, '-');                                      // Enters file name song
        //_name_file.erase(_name_file.begin(), _name_file.begin() + 1);       // Erase fisrt caracther
        // Falta afegir namefile i temps a la funcio
        cin >> _duracio;

        _singalone.createNewTrack(_name_track, _name_album, _name_artista, _duracio, _name_file);
        _singalone.includeTrackOnAlbum(_name_artista, _name_track, _name_album);    
	}
	cin >> _num;
	cin.ignore();
    for (int i = 0; i < _num; i++) {
        
		
        getline(cin, _name_user); 
    }
	
    cin >> _num;
	cin.ignore();
    for (int i = 0; i < _num; i++) {
        getline(cin, _name_user, '-');   
		// Enters artist
        //_name_artista.erase(_name_artista.begin(),_name_artista.begin()+1); // Erase first caracther
        getline(cin, _name_artista);
		
		
		_singalone.createNewUser(_name_user,_name_artista);
    }
	
	cout << _singalone.catalog();
}