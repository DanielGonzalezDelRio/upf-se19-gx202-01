#include "Album.h"

	string Album::title() {
		return _name;
	}
	void Album::title(string nameLocal) {
		_name = nameLocal;
	}
	bool Album::isListed() {
		return _listed;
	}
	void Album::list() {
		_listed = true;
	}
	void Album::unlist() {
		_listed = false;
	}
	
	///crear test para estas clases
	void Album::addTrack( Track * track){
		_tracks.push_back(track);
	}
	string Album::tracks() {
		string answer = "";
		int count = 1;
		for (Tracks::iterator it = _tracks.begin(); it != _tracks.end(); ++it) {
			answer.append(to_string(count)+" - "+(*it)->title()+ " ["+to_string((*it)->duration())+"s]\n");
			count++;
		}
		return answer;
	}
	Album::~Album(){
	
	}
