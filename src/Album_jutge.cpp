#include <iostream>
#include <list>
#include <cstring>
using namespace std;


class Album{

private:
	std::string _name = "-- Untitled --";
	bool _listed = false;

public:
	Album(){}
	string title() {
		return _name;
	}
	void title(string nameLocal) {
		_name = nameLocal;
	}
	bool isListed() {
		return _listed;
	}
	void list() {
		_listed = true;
	}
	void unlist() {
		_listed = false;
	}

};

typedef list<Album*> Albumes;

int main(void){

    Albumes _albums;
		int _num_albums = 0;

		string _name_album;
		char _stat = ' ';
		cin >> _num_albums;
		for (int j = 0 ; j < _num_albums ; j++){
			getline(cin,_name_album, '-');
			_name_album.erase(_name_album.begin(),_name_album.begin()+1);
			cin >> _stat;

			Album* _album_obj = new Album();

			_album_obj->title(_name_album);
			if (_stat == 'Y'){
				_album_obj->list();
			}
			else {
				_album_obj->unlist();
			}

			_albums.push_back(_album_obj);
		}
		for (Albumes::iterator it = _albums.begin(); it != _albums.end(); it++){
			if ((*it)->isListed() == true){
				cout << "Album: " << (*it)->title() << " [listed]\n";
			}
			else {
				cout << "Album: " << (*it)->title() << " [unlisted]\n";
			}

		}
	}
