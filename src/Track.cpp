#include "Track.h"
	std::string Track::title() {
		return _name;
	}
	void Track::title(std::string nameLocal) {
		_name = nameLocal;
	}
	unsigned int Track::duration() {
		return _duration;
	}
	void Track::duration(unsigned int duration) {
		_duration = duration;
	}
	std::string Track::master() {
		return _master;
	}
	void Track::master(std::string master) {
		_master = master;
	}
