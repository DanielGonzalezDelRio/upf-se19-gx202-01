#include "MiniCppUnit.hxx"
#include "ConverterGroup.hxx"
#include "Mp3Converter.hxx"


class ConverterGroupTest : public TestFixture<ConverterGroupTest>
{
public:
TEST_FIXTURE( ConverterGroupTest )
{
        TEST_CASE( testConvert_withMp3Converter );
        TEST_CASE( testConvert_withoutConverter );
        TEST_CASE( testConvert_withOggConverter );
        TEST_CASE( testConvert_withOggConverterAndDifferentBitrate );
        TEST_CASE( testConvert_withMp3AndOggConverters );
        TEST_CASE( testConvert_withUnknownConverter );
}

	/**
	 * setUp is automatically called before each test case
	 */
	void setUp()
	{
		LibFileSystem::createNetDirectory( "masters" );
		LibFileSystem::createNetDirectory( "compressed" );
		LibFileSystem::createNetDirectory( "config" );
	}

	/**
	 * tearDown is automatically called after each test case
	 */
	void tearDown()
	{
		LibFileSystem::cleanupDirectory( "masters" );
		LibFileSystem::cleanupDirectory( "compressed" );
		LibFileSystem::cleanupDirectory( "config" );
	}

	void createMasterFile( const std::string & name, int duration )
	{
		std::string completeName( "masters/" );
		completeName += name;
		std::ofstream os( completeName.c_str() );
		os << duration << std::endl;
	}

void testConvert_withMp3Converter() {
		ConverterGroup converterGroup;
        createMasterFile( "Master.wav", 50 );
		
		converterGroup.addConvertGroup("mp3" , 128 );
        converterGroup.convert( "masters/Master.wav", "compressed/Prefix" );

        ASSERT_EQUALS(
				"compressed/Prefix [128].mp3\n",
    			LibFileSystem::listDirectoryInOrder( "compressed" )
    		);
} 

void testConvert_withoutConverter() {
		ConverterGroup converterGroup;
        createMasterFile( "Master.wav", 50 );
		
        converterGroup.convert( "masters/Master.wav", "compressed/Prefix" );

        ASSERT_EQUALS(
				"",
    			LibFileSystem::listDirectoryInOrder( "compressed" )
    		);
}
void testConvert_withOggConverter() {
		ConverterGroup converterGroup;
        createMasterFile( "Master.wav", 50 );
		
		converterGroup.addConvertGroup("ogg" , 128 );
        converterGroup.convert( "masters/Master.wav", "compressed/Prefix" );

        ASSERT_EQUALS(
				"compressed/Prefix [128].ogg\n",
    			LibFileSystem::listDirectoryInOrder( "compressed" )
    		);
} 

void testConvert_withOggConverterAndDifferentBitrate() {
		ConverterGroup converterGroup;
        createMasterFile( "Master.wav", 50 );
		
		converterGroup.addConvertGroup("ogg" , 94 );
        converterGroup.convert( "masters/Master.wav", "compressed/Prefix" );

        ASSERT_EQUALS(
				"compressed/Prefix [94].ogg\n",
    			LibFileSystem::listDirectoryInOrder( "compressed" )
    		);
} 

void testConvert_withMp3AndOggConverters() {
		ConverterGroup converterGroup;
        createMasterFile( "Master.wav", 50 );
		
		converterGroup.addConvertGroup("ogg" , 94 );
		converterGroup.addConvertGroup("mp3" , 128 );
		converterGroup.addConvertGroup("mp3" , 96 );
		converterGroup.addConvertGroup("ogg" , 100 );
        converterGroup.convert( "masters/Master.wav", "compressed/Prefix" );

        ASSERT_EQUALS(
				"compressed/Prefix [100].ogg\n"
				"compressed/Prefix [128].mp3\n"
				"compressed/Prefix [94].ogg\n"
				"compressed/Prefix [96].mp3\n",
    			LibFileSystem::listDirectoryInOrder( "compressed" )
    		);
} 

void testConvert_withUnknownConverter() {
		ConverterGroup converterGroup;
        createMasterFile( "Master.wav", 50 );
        try{
			converterGroup.addConvertGroup("wav" , 94 );
			converterGroup.convert( "masters/Master.wav", "compressed/Prefix" );
			FAIL( "An exception should be caught!" );
		}
		catch( std::exception & e ){
			ASSERT_EQUALS( "Unsupported format", e.what() );
		}
} 


};
REGISTER_FIXTURE( ConverterGroupTest )
