#include "MiniCppUnit.hxx"
#include "Mp3Converter.hxx"

class Mp3ConverterTest : public TestFixture<Mp3ConverterTest>
{
public:
TEST_FIXTURE( Mp3ConverterTest )
{
        TEST_CASE( testConvert_generateFile );
        TEST_CASE( testConvert_generateContent )
        TEST_CASE( testConvert_withDifferentBitrate );
        TEST_CASE( testConvert_withUnsupportedFormat );
        TEST_CASE( testConvert_withInexistentMaster );
        TEST_CASE( testConvert_polymorphicCall );
}

	/**
	 * setUp is automatically called before each test case
	 */
	void setUp()
	{
		LibFileSystem::createNetDirectory( "masters" );
		LibFileSystem::createNetDirectory( "compressed" );
		LibFileSystem::createNetDirectory( "config" );
	}

	/**
	 * tearDown is automatically called after each test case
	 */
	void tearDown()
	{
		LibFileSystem::cleanupDirectory( "masters" );
		LibFileSystem::cleanupDirectory( "compressed" );
		LibFileSystem::cleanupDirectory( "config" );
	}

	void createMasterFile( const std::string & name, int duration )
	{
		std::string completeName( "masters/" );
		completeName += name;
		std::ofstream os( completeName.c_str() );
		os << duration << std::endl;
	}

void testConvert_generateFile() {
        Mp3Converter converter;
        createMasterFile( "Master.wav", 50 );
        converter.convert( "masters/Master.wav", "compressed/Prefix" );

        ASSERT_EQUALS(
    			"compressed/Prefix [128].mp3\n",
    			LibFileSystem::listDirectoryInOrder( "compressed" )
    		);
}
void testConvert_generateContent() {
        Mp3Converter converter;
        createMasterFile( "Master.wav", 50 );
        converter.convert( "masters/Master.wav", "compressed/Prefix" );

        ASSERT_EQUALS(
          "MP3 extracted from 'masters/Master.wav' at 128 bps and length 50s.\n",
          LibFileSystem::fileContent( "compressed/Prefix [128].mp3" )
        );
}
void testConvert_withDifferentBitrate() {
  Mp3Converter converter;
  createMasterFile( "Master.wav", 50 );
  converter.bitRate(96);
  converter.convert( "masters/Master.wav", "compressed/Prefix" );

  ASSERT_EQUALS(
    "MP3 extracted from 'masters/Master.wav' at 96 bps and length 50s.\n",
    LibFileSystem::fileContent( "compressed/Prefix [96].mp3" )
  );
}
void testConvert_withUnsupportedFormat() {

    Mp3Converter converter;
    createMasterFile( "Master.wav", 50 );
		try
		{
      converter.bitRate(90);
			FAIL( "An exception should be caught!" );
		}
		catch( std::exception & e )
		{
			ASSERT_EQUALS( "Unsupported format", e.what() );
		}

}
void testConvert_withInexistentMaster() {
  Mp3Converter converter;

  try
  {
  converter.convert( "masters/Master.wav", "compressed/Prefix" );
    FAIL( "An exception should be caught!" );
  }
  catch( std::exception & e )
  {
    ASSERT_EQUALS( "The master file does not exist", e.what() );
  }
}
void testConvert_polymorphicCall() {
  Converter *convert_poli;
  Mp3Converter converter;
  convert_poli = &converter;
  try
  {
  convert_poli->convert( "masters/Master.wav", "compressed/Prefix" );
    FAIL( "An exception should be caught!" );
  }
  catch( std::exception & e )
  {
    ASSERT_EQUALS( "The master file does not exist", e.what() );
  }
}
};
REGISTER_FIXTURE( Mp3ConverterTest )
