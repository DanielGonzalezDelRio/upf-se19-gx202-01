#include "MiniCppUnit.hxx"
#include "Album.cpp"
#include "Track.h"

class AlbumTests : public TestFixture<AlbumTests>
{
public:
	TEST_FIXTURE( AlbumTests )
	{
	TEST_CASE( testTitle_byDefault );
	TEST_CASE( testTitle_afterModifying );
	TEST_CASE( testListed_byDefault );
	TEST_CASE( testListed_afterListing );
	TEST_CASE( testListed_afterUnlisting );
	TEST_CASE( testTracks_withoutTracks );
	TEST_CASE( testTracks_withoneTrack );
	}

	void testTitle_byDefault()
	{
		Album album;
		ASSERT_EQUALS( "-- Untitled --", album.title() );
	}

	void testTitle_afterModifying()
	{
		Album album;
		album.title( "A title" );
		ASSERT_EQUALS( "A title", album.title() );
	}

	void testListed_byDefault()
	{
		Album album;
		ASSERT_EQUALS( false, album.isListed() );
	}

	void testListed_afterListing()
	{
		Album album;
		album.list();
		ASSERT_EQUALS( true, album.isListed() );
	}

	void testListed_afterUnlisting()
	{
		Album album;
		album.list();
		album.unlist();
		ASSERT_EQUALS( false, album.isListed() );
	}
	void testTracks_withoutTracks()
	{
		Album album;
		ASSERT_EQUALS( "", album.tracks() );
	}
	void testTracks_withoneTrack()
	{
		Album album;
		Track _track_obj;
		_track_obj.title("a song");
		_track_obj.master("asong.wav");
		_track_obj.duration(15u);
		album.addTrack(&_track_obj);
		
		ASSERT_EQUALS( "1 - a song [15s]\n", album.tracks() );
		
	}

};

REGISTER_FIXTURE( AlbumTests )
