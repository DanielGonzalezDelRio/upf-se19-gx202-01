#include "MiniCppUnit.hxx"
#include "Artist.cpp"

class ArtistTests : public TestFixture<ArtistTests>
{
public:
	TEST_FIXTURE( ArtistTests )
	{
	TEST_CASE( testName_byDefault );
	TEST_CASE( testName_aftername );
	TEST_CASE( testSingle_byDefault );
	TEST_CASE( testSingleGroup_afterbeGroup );
	TEST_CASE( testDescriptio_byDefault );
	TEST_CASE( testDescriptio_afterNameAndbeGroup );
	TEST_CASE( testTrackVector_empty );
	TEST_CASE( testTrackVector_withOneTrack );
	TEST_CASE( testAlbumVector_empty );
	TEST_CASE( testAlbumVector_withOneAlbum );
	}

	void testName_byDefault()
	{
		Artist artist;
		ASSERT_EQUALS( "-- Untitled --", artist.name() );
	}

	void testName_aftername()
	{
		Artist artist;
		artist.name("Mago de Oz");
		ASSERT_EQUALS( "Mago de Oz", artist.name() );
	}

	void testSingle_byDefault()
	{
		Artist artist;
		ASSERT_EQUALS( true, artist.isSingle() );
	}

	void testSingleGroup_afterbeGroup()
	{
		Artist artist;
		artist.beSolo();
		artist.beGroup();
		ASSERT_EQUALS( false, artist.isSingle() );
	}

	void testDescriptio_byDefault()
	{
		Artist artist;
		ASSERT_EQUALS( "-- Untitled -- [solo]\n" , artist.description() );
	}
	
	void testDescriptio_afterNameAndbeGroup()
	{
		Artist artist;
		artist.name("Mago de Oz");
		artist.beGroup();
		ASSERT_EQUALS( "Mago de Oz [group]\n" , artist.description() );
	}
	void testTrackVector_empty()
	{
		Artist artist;
		ASSERT_EQUALS( "" , artist.tracks() );
	}
	
	void testTrackVector_withOneTrack()
	{
		Artist artist;
		artist.addNewTrack("A track", "aMasterFile.wav",30u);
		ASSERT_EQUALS( "\tA track [30s]\n\t\tmasters/aMasterFile.wav\n", artist.tracks() );
	}
	
	void testAlbumVector_empty()
	{
		Artist artist;
		ASSERT_EQUALS( "" , artist.albums() );
	}
	
	void testAlbumVector_withOneAlbum()
	{
		Artist artist;
		artist.addNewAlbum("An album");
		ASSERT_EQUALS( "Album: An album [unlisted]\n", artist.albums() );
	}
	
};

REGISTER_FIXTURE( ArtistTests )
