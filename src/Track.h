#ifndef Track_h
#define Track_h
class Track{
private:
	std::string _name = "-- Untitled --";
	unsigned int _duration = 0u;
	std::string _master = "";

public:
	std::string title();
	void title(std::string nameLocal);
	unsigned int duration();
	void duration(unsigned int duration);
	std::string master();
	void master(std::string master);
};
#endif