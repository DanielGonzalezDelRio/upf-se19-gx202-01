//#include "User.h"
class User{
private:
	string _name = "-- Untitled --";
	string _mail = "-- Untitled --";
public:
	string name() {
		return _name;
	}
	void name(std::string nameLocal) {
		_name = nameLocal;
	}
	string mail() {
		return _mail;
	}
	void mail(std::string nameLocal) {
		_mail = nameLocal;
	}
	string data() {
		return (_name + " <" + _mail + ">\n");
	}
};