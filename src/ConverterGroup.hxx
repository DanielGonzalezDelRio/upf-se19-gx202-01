#ifndef ConverterGroup_h
#define ConverterGroup_h
#include <list>
#include <iostream>
#include <string>
#include <cstring>
#include <fstream>
#include "Converter.hxx"
#include "Exceptions.h"
#include "LibFileSystem.hxx"
#include "Mp3Converter.hxx"
#include "OggConverter.hxx"

using namespace std;
typedef list<Converter*> Converters;

class ConverterGroup{
	private: 
		Converters _converters;
	public:
		void addConvertGroup( string _name, int btr);
		virtual void convert(string name, string name2);
		~ConverterGroup();
};
#endif
