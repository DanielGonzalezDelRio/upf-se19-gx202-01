#ifndef Mp3Converter_h
#define Mp3Converter_h
#include <list>
#include <iostream>
#include <string>
#include <cstring>
#include <fstream>
#include "Converter.hxx"
#include "Exceptions.h"
#include "LibFileSystem.hxx"
#include "FraunhofferTools.hxx"
using namespace std;

class Mp3Converter : public Converter {
private:
  int _Rate = 128;
  public /*virtual Converter{}*/:
  void setUp();
  void tearDown();
  void createMasterFile( const std::string & name, int duration );
  void convert(string name, string name2);
  void bitRate(int rate);
};
#endif
