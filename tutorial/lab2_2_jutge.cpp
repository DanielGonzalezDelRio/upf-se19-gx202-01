#include <iostream>
#include <string>
#include <stdio.h>
#include <string.h>
using namespace std;

int main(void)
{
	int numtracks = 0;
	cin >> numtracks;
  string n = "";
  string t = "";
	getline(cin,t);
	for (int i= 0; i <numtracks; i++) {
    getline(cin, n, '-');
    getline(cin, t);
		cout << "Track: " <<n << " ["<< t <<"s]\n";
	}
	return 0;
}
