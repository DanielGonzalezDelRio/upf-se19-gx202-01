#ifndef animals_hxx
#define animals_hxx

/*
   - Now we add a new attribute of type std::string
   - Add getters and setters
 */

#include <iostream>
#include <string>

class Animal
{
public:
	virtual void writeSpecies() const
	{
		std::cout << "...undefined..." << std::endl;
	}
	void name(std::string theName)
	{
		_name = theName;
	}
	std::string& name()
	{
		return _name;
	}

private:
	std::string _name;
};

class Elephant : public Animal
{
public:
	void writeSpecies() const
	{
		std::cout << "elephant" << std::endl;
	}
};

class Frog : public Animal
{
public:
	void writeSpecies() const
	{
		std::cout << "frog" << std::endl;
	}
};

class Cat : public Animal
{
public:
	void writeSpecies() const
	{
		std::cout << "cat" << std::endl;
	}
};

#endif
#include <list>
#include "animals.hxx"
using namespace std;

typedef list<Animal*> Animals;
// Free function
// Parameter passing by reference
void writeInfo( const Animal& anAnimal )
{
        anAnimal.writeSpecies();
}

int main(void)
{
  Animals animals;

	// introduce one of each type
	Animal anAnimal;
	anAnimal.name("Pikatxu");
	animals.push_back(&anAnimal);

	Elephant anElephant;
	anElephant.name("Jumbo");
	animals.push_back(&anElephant);

	Frog aFrog;
	aFrog.name("Kermit");
	animals.push_back(&aFrog);

	Cat aCat;
	aCat.name("Puss in Boots");
	animals.push_back(&aCat);


	// iterate using iterators
	for (Animals::iterator it=animals.begin(); it!=animals.end(); it++)
	{
		std::cout << (*it)->name() << " is of the species ";
		(*it)->writeSpecies();
	}

	return 0;
}
